#!/bin/sh

set -e

git init

git config commit.template .git-commit-template

HOOKS={{cookiecutter.use_hooks}}

# The "consumer" and "http" - type options alias to "rmq" and "pyramid"
# here. If in a later stadium the default changes, we can alter this here.

TYPE={{cookiecutter.type}}

if [ "$HOOKS" = "yes" ]; then
    git config core.hooksPath bin/git-hooks
fi

case "$TYPE" in
    http)
        cp -r .cookiecutter/type-pyramid/package/* {{cookiecutter.package_name}}/
        cp .cookiecutter/type-pyramid/development.ini ./
        cp .cookiecutter/type-pyramid/package.conf.dist ./
        cp .cookiecutter/type-pyramid/development.ini.license ./
        cp .cookiecutter/type-pyramid/package.conf.dist.license ./
        cp .cookiecutter/type-pyramid/.gitlab-ci.yml ./
        ;;
    consumer)
        cp -r .cookiecutter/type-rmq/package/* {{cookiecutter.package_name}}/
        cp .cookiecutter/type-rmq/application_config.json ./
        cp .cookiecutter/type-rmq/config.json.dist ./
        cp .cookiecutter/type-rmq/application_config.json.license ./
        cp .cookiecutter/type-rmq/config.json.dist.license ./
        cp .cookiecutter/type-rmq/.gitlab-ci.yml ./
        ;;
    python)
        cp .cookiecutter/type-python/.gitlab-ci.yml ./
        ;;
esac

# Remove temporarily cookiecutter directory
rm -Rf .cookiecutter

git add -A
git commit --no-verify \
    -m "Initial import of {{cookiecutter.repo_name}}"
echo
echo
echo
echo "-------"
echo
echo "# Initialized git and added first commit:"
echo
git log
echo
echo "-------"
echo
echo "# Make sure to create a project in gitlab, inspiration:"
echo "$ git remote add origin git@gitlab.com:{{cookiecutter.gitlab_username}}/{{cookiecutter.repo_name}}.git" 
echo "$ git push -u origin master"

exit 0
