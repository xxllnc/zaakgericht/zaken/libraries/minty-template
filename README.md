# Cookiecutter Package for easy Python development

This repository allows
[Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/) users to
generate an empty python based repository for rapid development.

### TL/DR:

    !!! Do Read: "Important for creating a new service" below !!!

### Usage

    * install cookiecutter on your system:
    pip3 install cookiecutter

    * in your start.git repo directory create the template:
    cookiecutter https://gitlab.com/minty-python/minty-template.git

    * Add the correct entries of the new repository you created in start/.mrconfig

    * Create your usual virtual environment with the `mr zs python`-utilities
---

### Important for creating a new service:

#### First step when creating a new repo:

- Use only dashes, not underscores in repository names
- Check consistent use of plural names: consumer/consumers, event/events etc
- Be consistent with zaaksysteem- or zsnl- prefixes
- Disable the shared runners
- Enable 2FA
- Make sure only maintainers can push to master


#### Ask ops:

When creating a new service, some configuration and resources need to be created in our infrastructure.
This is done by our devops team and can be requested through an internal "zaak" in mintlab.zaaksysteem.nl.
To do this, have a look in [this wiki article](https://mintlab.atlassian.net/wiki/spaces/ML/pages/1291714562/Requesting+a+new+service+on+our+platform)
to provide the required information.


---

### Contents of a package after generating with cookiecutter

* Python 3.7
* ipython
* Pytest testing
* CI tooling for gitlab
* Configuration for flake8, black and isort

**Optional**

* When type "http" is selected, this will default to a Pyramid setup
* When type "consumer" is selected this will default to a RabbitMQ setup
* When type "pytho" is selected this will install a plain python package

---

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/minty-python/minty/blob/master/CONTRIBUTING.md)
for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](https://semver.org/) for versioning. For the versions
available, see the
[tags on this repository](https://gitlab.com/minty-python/minty/tags/).

## License

Copyright (c) 2018, Mintlab B.V. and all persons listed in the
[CONTRIBUTORS.md](https://gitlab.com/minty-python/minty/blob/master/CONTRIBUTORS.rst)
file.

This project is licensed under the EUPL, v1.2. See the
[EUPL-1.2.txt](https://gitlab.com/minty-python/minty/blob/master/LICENSES/EUPL-1.2.txt)
file for details.
---
