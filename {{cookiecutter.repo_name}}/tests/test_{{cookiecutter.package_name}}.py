# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

class TestExample:
    """Example of a simple test"""

    def test_hello_world(self):
        """Hello World"""
        assert "Hello World" == "Hello World"
